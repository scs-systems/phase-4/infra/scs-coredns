#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi
kubectl -n infra create configmap scs-coredns-config-$SCSREL --from-file=configmap/
