#!/bin/bash
if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi

NAMESPACE=infra
APP=scs-coredns
IMAGE=gitlab.scsuk.net:5005/scs-systems/ext_registry/coredns/coredns:1.5.0

set -x # 
cat <<EOT | kubectl apply -n "$NAMESPACE" --force -f -
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: $APP
  labels:
    app: $APP
spec:
  selector:
    matchLabels:
      app: $APP
  template:
    metadata:
      labels:
        app: $APP
    spec:
      containers:
      - name: $APP
        image: $IMAGE
        args: ["-conf", "/mnt/coredns/Corefile"]
        ports:
        - containerPort: 53
          protocol: UDP
        - containerPort: 53
          protocol: TCP
        volumeMounts:
        - name: scs-coredns-config
          mountPath: "/mnt/coredns"
      volumes:
      - name: scs-coredns-config
        configMap:
          name: scs-coredns-config-$SCSREL
      hostNetwork: true
EOT

echo
kubectl -n infra get ds $APP
echo
kubectl -n infra get pods -l app=$APP -o wide
